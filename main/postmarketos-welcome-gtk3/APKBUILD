# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
pkgname=postmarketos-welcome-gtk3
pkgver=0.2.1
pkgrel=0
pkgdesc="postmarketOS welcome app"
url="https://gitlab.com/postmarketOS/postmarketos-welcome-gtk3"
arch="all"
license="GPL-3.0-or-later"
makedepends="meson libhandy-dev"
source="https://gitlab.com/postmarketOS/postmarketos-welcome-gtk3/-/archive/$pkgver/postmarketos-welcome-gtk3-$pkgver.tar.bz2"
options="!check" # No tests

build() {
	abuild-meson . output
	meson compile ${JOBS:+-j ${JOBS}} -C output
}

package() {
	DESTDIR="$pkgdir" meson install -C output

	install -Dm644 "data/welcome-message" \
		"$pkgdir/etc/welcome-message"

	# Autostart on first login, the app removes the symlink when started
	local _autostartdir="$pkgdir/etc/skel/.config/autostart"
	mkdir -p "$_autostartdir"
	ln -s "/usr/share/applications/org.postmarketos.Welcome.desktop" \
		"$_autostartdir/org.postmarketos.Welcome.desktop"
}

sha512sums="fa31267e326e849195039fd4cd9d43bc80bc19449d200c2f98df9bea55bd93c2a27089c9f484d1a3abb2dfbd1e4dc825cec94461db20accc1af877ecdf09b90d  postmarketos-welcome-gtk3-0.2.1.tar.bz2"
