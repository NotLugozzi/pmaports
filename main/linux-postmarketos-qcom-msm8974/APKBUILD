# Maintainer: Luca Weiss <luca@z3ntu.xyz>
_flavor=postmarketos-qcom-msm8974
_config="config-$_flavor.$CARCH"

pkgname=linux-$_flavor
pkgver=5.9.0_rc4
pkgrel=0
_commit="0aa15376cf66e41bb980b24a388cdf2dcb770a68"
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8974 devices"
arch="armv7"
_carch="arm"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="
	bison
	findutils
	flex
	installkernel
	openssl-dev
	perl
	"
source="https://gitlab.com/postmarketOS/linux-postmarketos/-/archive/$_commit/linux-postmarketos-$_commit.tar.gz
	config-$_flavor.armv7
	"
builddir="$srcdir/linux-postmarketos-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"
}

sha512sums="77e13b23fdbaf36cc91b3c55b9c5f9f7bec6c5c31c6d255daad18e0c1e27176218d465a03c449bd3678f7cb37b5a21970145fc6a8dac5400f9eb318281157f47  linux-postmarketos-0aa15376cf66e41bb980b24a388cdf2dcb770a68.tar.gz
8c918d1fa9fdda74829b7df5d3b9fb91536e8430e9d689d0eb515336cba5bf0ac09f0d0810bac6331ef76ecb5cdb119a9c20d1c10ee22234513ee5bee2cc1fa5  config-postmarketos-qcom-msm8974.armv7"
